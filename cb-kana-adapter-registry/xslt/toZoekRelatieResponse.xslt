<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:if="http://nl.consumentenbond.services/adapters/KanaAdapterService/v1/IF">

	<xsl:template match="/">
		<xsl:apply-templates select="jsonObject"/>
		<xsl:apply-templates select="jsonArray/jsonElement"/>
	</xsl:template>

	<xsl:template match="jsonObject">
		<if:zoekRelatieResponse>
			<xsl:apply-templates select="relaties"/>
		</if:zoekRelatieResponse>
	</xsl:template>
	
	<xsl:template match="relaties">
        <if:relatie>
            <xsl:if test="relatienummer != ''">
                <if:relatienummer>
                    <xsl:value-of select="relatienummer" />
                </if:relatienummer>
            </xsl:if>
            <xsl:if test="relatietype != ''">
                <if:relatietype>
                    <xsl:value-of select="relatietype" />
                </if:relatietype>
            </xsl:if>
            <xsl:if test="voornaam != ''">
                <if:voornaam>
                    <xsl:value-of select="voornaam" />
                </if:voornaam>
            </xsl:if>
            <xsl:if test="voorletters != ''">
                <if:voorletters>
                    <xsl:value-of select="voorletters" />
                </if:voorletters>
            </xsl:if>
            <xsl:if test="tussenvoegsels != ''">
                <if:tussenvoegsels>
                    <xsl:value-of select="tussenvoegsels" />
                </if:tussenvoegsels>
            </xsl:if>

            <xsl:if test="achternaam != ''">
                <if:achternaam>
                    <xsl:value-of select="achternaam" />
                </if:achternaam>
            </xsl:if>

            <!-- TODO datum conversie? -->
            <xsl:if test="geboortedatum != ''">
                <if:geboortedatum>
                    <xsl:value-of select="geboortedatum" />
                </if:geboortedatum>
            </xsl:if>

            <xsl:if test="overlijdensdatum != ''">
                <if:overlijdensdatum>
                    <xsl:value-of select="overlijdensdatum" />
                </if:overlijdensdatum>
            </xsl:if>

            <!-- TODO mapping?? -->
            <xsl:if test="geslacht != ''">
                <if:geslacht>
                    <xsl:value-of select="geslacht" />
                </if:geslacht>
            </xsl:if>

            <if:emailadres>
                <xsl:value-of select="emailadres" />
            </if:emailadres>

            <xsl:if test="status != ''">
                <if:status>
                    <xsl:value-of select="status" />
                </if:status>
            </xsl:if>

            <xsl:if test="adressen != ''">
                <if:adressen>
                     <xsl:apply-templates select="adressen" />
               </if:adressen>
            </xsl:if>

            <xsl:apply-templates select="facturatiegegevens" />
            
            <xsl:if test="telefoon != ''">
               <if:contactgegevens>
	               <if:contacttype>
		                <xsl:text>TEL_PRIMAIR</xsl:text>
		            </if:contacttype>
		
	                <if:telefoon>
	                    <xsl:value-of select="telefoon" />
	                </if:telefoon>
                    
               </if:contactgegevens>
            </xsl:if>
            
            <xsl:if test="contactkanalen != ''">
                <if:contactkanalen>
                    <xsl:apply-templates select="contactkanalen" />
                </if:contactkanalen>
            </xsl:if>
            
            <xsl:apply-templates select="kenmerken" />

        </if:relatie>
    </xsl:template>

    <xsl:template match="adressen">
        <if:adres>
            <xsl:if test="straatnaam != ''">
                <if:straatnaam>
                    <xsl:value-of select="straatnaam" />
                </if:straatnaam>
            </xsl:if>

            <xsl:if test="huisnummer != ''">
                <if:huisnummer>
                    <xsl:value-of select="huisnummer" />
                </if:huisnummer>
            </xsl:if>

            <xsl:if test="toevoeging != ''">
                <if:toevoeging>
                    <xsl:value-of select="toevoeging" />
                </if:toevoeging>
            </xsl:if>

            <xsl:apply-templates select="postcode" />

            <xsl:if test="plaatsnaam != ''">
                <if:plaatsnaam>
                    <xsl:value-of select="plaatsnaam" />
                </if:plaatsnaam>
            </xsl:if>

            <if:adrestype>
                <xsl:value-of select="type" />
            </if:adrestype>

            <if:postbus>
                <xsl:call-template name="mapToBoolean">
                    <xsl:with-param name="JaNeeValue" select="postbus" />
                </xsl:call-template>
            </if:postbus>

            <xsl:if test="opmerking!=''">
                <if:opmerking>
                    <xsl:value-of select="opmerking" />
                </if:opmerking>
            </xsl:if>
        </if:adres>
    </xsl:template>

    <xsl:template match="postcode">
        <if:postcode>
            <if:postcode>
                <xsl:value-of select="postcode" />
            </if:postcode>
            <if:land>
                <xsl:value-of select="land" />
            </if:land>
        </if:postcode>
    </xsl:template>

    <xsl:template match="facturatiegegevens">
        <if:facturatiegegevens>
            <if:IBAN>
                <xsl:value-of select="IBAN" />
            </if:IBAN>
            <if:incasso>
                <xsl:call-template name="mapToBoolean">
                    <xsl:with-param name="JaNeeValue" select="postbus" />
                </xsl:call-template>
            </if:incasso>
        </if:facturatiegegevens>
    </xsl:template>

        <xsl:template match="contactkanalen">
        <xsl:apply-templates select="contactkanaal"/>
    </xsl:template>
    
    <xsl:template match="contactkanaal">
        <if:contactkanaal>
            <xsl:value-of select="." />
        </if:contactkanaal>
    </xsl:template>

    <xsl:template match="kenmerken">
        <if:relatieKenmerken>
            <if:panel>
                <xsl:call-template name="mapToBoolean">
                    <xsl:with-param name="JaNeeValue" select="panel" />
                </xsl:call-template>
            </if:panel>
            <if:advertentieVrij>
                <xsl:call-template name="mapToBoolean">
                    <xsl:with-param name="JaNeeValue" select="advertentievrij" />
                </xsl:call-template>
            </if:advertentieVrij>
            <if:korting>
                <xsl:call-template name="mapToBoolean">
                    <xsl:with-param name="JaNeeValue" select="korting" />
                </xsl:call-template>
            </if:korting>
        </if:relatieKenmerken>
    </xsl:template> 
    
    <xsl:template name="mapToBoolean">
        <xsl:param name="JaNeeValue" />

        <xsl:choose>
            <xsl:when test="$JaNeeValue='JA'">
                <xsl:text>true</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>false</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    	
	<xsl:template match="@* | node()" priority="-1"/>
</xsl:stylesheet>